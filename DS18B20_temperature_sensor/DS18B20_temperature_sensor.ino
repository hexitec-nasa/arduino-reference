// This is an example sketch on how to read the 
// DS18B20 Digital temperature sensor 
// see https://www.adafruit.com/product/374
// the datasheet is available 
// https://cdn-shop.adafruit.com/datasheets/DS18B20.pdf

// Hook up guide
//- DS18B20:
//     + connect VCC (3.3V) to the appropriate DS18B20 pin (VDD)
//     + connect GND to the appopriate DS18B20 pin (GND)
//     + connect D4 to the DS18B20 data pin (DQ)
//     + connect a 4.7K resistor between DQ and VCC.

// good tutorial
// http://www.jerome-bernard.com/blog/2015/10/04/wifi-temperature-sensor-with-nodemcu-esp8266/

// from https://github.com/PaulStoffregen/OneWire
#include <OneWire.h>
// from https://github.com/milesburton/Arduino-Temperature-Control-Library
#include <DallasTemperature.h>

#define BAUD 9600


// define where the DS18B20 are hooked up
#define ONE_WIRE_BUS D4
// set the temperature resolution to number of bits (9 to 12)
#define TEMP_RESOLUTION 12

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature DS18B20(&oneWire);

// a variable to store the total number of sensors found
int numberOfDS18B20;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(BAUD);

  // Start up Dallas OneWire Sensors
  DS18B20.begin();
  // Search for all connected OneWire Sensors
  numberOfDS18B20 = discoverOneWireDevices();
  Serial.print("Found "); Serial.print(numberOfDS18B20); Serial.println(" DS18B20 sensors");
  DS18B20.setResolution(TEMP_RESOLUTION);
}

void loop() {
  // put your main code here, to run repeatedly:

  // Request from OneWire sensor(s)
  float DS18B20_temperature;
  DS18B20.requestTemperatures();

  for (int i = 0; i < numberOfDS18B20; i++) {
    //if (i == 3) {lcd.setCursor(0, 1);}
    DS18B20_temperature = DS18B20.getTempCByIndex(i);
    //lcd.print(temp, 1);
    //lcd.print(" ");
    Serial.print("Sensor ");
    Serial.print(i); Serial.print(": "); Serial.print(DS18B20_temperature); Serial.println(" C");
    delay(100);
  }
}


int discoverOneWireDevices(void) {
  // Searches for connected OneWire devices
  // Reference
  // http://www.hacktronics.com/Tutorials/arduino-1-wire-address-finder.html
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  int count = 0;
  while (oneWire.search(addr)) {
    for ( i = 0; i < 8; i++) {
      if (addr[i] < 16) {
      }
      if (i < 7) {
      }
    }
    if ( OneWire::crc8( addr, 7) != addr[7]) {
      return 0;
    }
    count++;
  }
  oneWire.reset_search();
  return count;
}